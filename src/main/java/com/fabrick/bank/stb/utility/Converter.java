package com.fabrick.bank.stb.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Converter {

    public static String fromObjectToJson(Object o) {

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(o);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
