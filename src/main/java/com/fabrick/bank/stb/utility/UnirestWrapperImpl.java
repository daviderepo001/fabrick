package com.fabrick.bank.stb.utility;

import com.fabrick.bank.stb.json.MoneyTransferRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.log4j.Log4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Log4j
@Component("UnirestWrapperImpl")
public class UnirestWrapperImpl implements UnirestWrapper {

    public String get(String url, int index) throws UnirestException {

        HttpResponse<JsonNode> jsonResponse = getResponse(url);
        if (jsonResponse == null) {
            log.warn("Responce is null");
            return null;
        }
        return jsonResponse.getBody().getObject().toString().substring(index);
    }

    /*
    public JSONObject post(String url, MoneyTransferRequest body) throws UnirestException {

        HttpResponse<JsonNode> jsonResponse = postResponse(url, body);
        if (jsonResponse == null) {
            log.warn("Responce is null");
            return null;
        }
        return jsonResponse.getBody().getObject();
    }
    */

    private HttpResponse<JsonNode> getResponse(String url) throws UnirestException {

        return Unirest
                .get(url)
                .header(Costants.API_KEY_NAME, Costants.API_KEY)
                .header(Costants.AUTH_SCHEMA_NAME, Costants.AUTH_SCHEMA)
                .asJson();
    }

    /*
    private HttpResponse<JsonNode> postResponse(String url, MoneyTransferRequest body) throws UnirestException {

        return Unirest
                .post(url)
                .header(Costants.X_TIME_ZONE_NAME, Costants.X_TIME_ZONE)
                .header(Costants.API_KEY_NAME, Costants.API_KEY)
                .header(Costants.AUTH_SCHEMA_NAME, Costants.AUTH_SCHEMA)
                .body(body)
                .asJson();
    }
    */

    public JSONObject post (String restUri, MoneyTransferRequest request) throws IOException {

        log.info("callRest starting..");
        String jsonResponse;
        HttpURLConnection conn = (HttpURLConnection) new URL(restUri).openConnection();
        conn.setReadTimeout(120000);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; utf-8");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);
        conn.setRequestProperty(Costants.X_TIME_ZONE_NAME, Costants.X_TIME_ZONE);
        conn.setRequestProperty(Costants.API_KEY_NAME, Costants.API_KEY);
        conn.setRequestProperty(Costants.AUTH_SCHEMA_NAME, Costants.AUTH_SCHEMA);

        ObjectMapper mapper = new ObjectMapper();

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonInputString = ow.writeValueAsString(request);

        OutputStream os = conn.getOutputStream();
        byte[] input = jsonInputString.getBytes("utf-8");
        os.write(input, 0, input.length);
        int restResponseCode = conn.getResponseCode();
        log.info("callRest - restResponseCode: " + restResponseCode);

        if (restResponseCode != 200) throw new IOException();

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        while ((jsonResponse = br.readLine()) != null) {
            return new JSONObject(jsonResponse);
        }
        return null;
    }
}