package com.fabrick.bank.stb.utility;

import com.fabrick.bank.stb.json.MoneyTransferRequest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;

public interface UnirestWrapper {

    public String get(String url, int index) throws Exception;
    public JSONObject post(String url, MoneyTransferRequest body) throws UnirestException, IOException;
}
