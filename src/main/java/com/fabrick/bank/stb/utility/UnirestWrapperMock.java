package com.fabrick.bank.stb.utility;

import com.fabrick.bank.stb.json.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

import java.util.Date;

import static com.fabrick.bank.stb.operations.OperationAccountBalance.API_URL_ACCOUNT_BALANCE_END;
import static com.fabrick.bank.stb.operations.OperationAccountBalance.API_URL_ACCOUNT_BALANCE_START;
import static com.fabrick.bank.stb.operations.OperationTransactions.API_URL_ACCOUNT;
import static com.fabrick.bank.stb.operations.OperationTransactions.API_URL_TRANSACTIONS;

public class UnirestWrapperMock implements UnirestWrapper {

    public String get(String url, int index) throws Exception {

        if (url.contains(API_URL_ACCOUNT_BALANCE_END)) {
            try {
                String subUrl1 = url.replaceAll(Costants.HOST_URL + API_URL_ACCOUNT_BALANCE_START, "");
                String subUrl2 = subUrl1.replaceAll(API_URL_ACCOUNT_BALANCE_END, "");
                long accountId = Long.valueOf(subUrl2);
                if (accountId == 0.0f) throw new Exception ("Invalid account identifier");
            } catch (Exception e) {
                throw new Exception ("Invalid account identifier");
            }
            AccountBalance accountBalance = new AccountBalance();
            accountBalance.setDate(new Date());
            accountBalance.setCurrency("EUR");
            accountBalance.setBalance(50f);
            accountBalance.setAvailableBalance(50f);
            Gson gsonBuilder = new GsonBuilder().create();
            return gsonBuilder.toJson(accountBalance);
        } else if (url.contains(API_URL_TRANSACTIONS)) {
            try {
                String subUrl1 = url.replaceAll(Costants.HOST_URL + API_URL_ACCOUNT, "");
                String subUrl2 = subUrl1.replaceAll(API_URL_TRANSACTIONS, "");
                int indexEndAccountId = subUrl2.indexOf("?");
                String subUrl3 = subUrl2.substring(0, indexEndAccountId);
                long accountId = Long.valueOf(subUrl3);
            } catch (Exception e) {
                return "Invalid account identifier";
            }
            Transaction transaction = new Transaction();
            transaction.setTransactionId("519157");
            transaction.setOperationId("00000000519157");
            transaction.setAccountingDate(new Date());
            transaction.setValueDate(new Date());
            transaction.setType(new Type("GBS_TRANSACTION_TYPE", "GBS_ACCOUNT_TRANSACTION_TYPE_0050"));
            transaction.setAmount(-586.39f);
            transaction.setCurrency("EUR");
            transaction.setDescription("PD VISA CORPORATE 11");
            Gson gsonBuilder = new GsonBuilder().create();
            return gsonBuilder.toJson(transaction);
        }
        return "Mock in Error";
    }

    //TODO prevedere casi in cui mancano parametri obbligatori
    public JSONObject post(String url, MoneyTransferRequest body) throws UnirestException{

        MoneyTransferResponse response = new MoneyTransferResponse();

        response.setMoneyTransferId("452516859427");
        response.setStatus("EXECUTED");
        response.setDirection("OUTGOING");
        response.setCreditor(new Creditor("John Doe", new Account("IT23A0336844430152923804660", "SELBIT2BXXX"), new Address(null, null, null)));
        response.setDebtor(new Debtor(null, new Account("IT61F0326802230280596327270", null)));
        response.setCro("1234566788907");
        response.setUri("REMITTANCE_INFORMATION");
        response.setTrn("AJFSAD1234566788907CCSFDGTGVGV");
        response.setDescription("Description");
        response.setCreatedDatetime(new Date());
        response.setAccountedDatetime(new Date());
        response.setDebtorValueDate(new Date());
        response.setCreatedDatetime(new Date());
        response.setAmount(new Amount(800, "EUR", 800, "EUR", new Date(2020, 05, 02), 1));

        return new JSONObject(response);
    }
}
