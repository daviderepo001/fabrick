package com.fabrick.bank.stb.utility;

import com.fabrick.bank.stb.json.Transaction;
import com.fabrick.bank.stb.model.ResponseData;
import com.fabrick.bank.stb.model.ResponseError;
import com.fabrick.bank.stb.model.ResponseVersion;
import com.fabrick.bank.stb.model.RestDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class CommonResponseHandler {

    public static ResponseEntity<RestDataResponse> generateResponse(RestDataResponse response, String timestamp) {

        if (response.getError().getCode().equals("")) {

            return ResponseEntity.ok()
                    .body(response);

        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(response);
    }

    public static ResponseEntity<RestDataResponse> generateResponse(String message, String timestamp) {

        RestDataResponse response = new RestDataResponse();
        response.setVersion(new ResponseVersion(timestamp));
        response.setData(message);

        return ResponseEntity.ok()
                .body(response);

    }

    public static ResponseEntity<RestDataResponse> generateResponse(ResponseData data, String timestamp) {

        RestDataResponse response = new RestDataResponse();

        if (data != null) {

            response.setVersion(new ResponseVersion(timestamp));
            response.setData(data);

            return ResponseEntity.ok().body(response);

        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public static ResponseEntity<RestDataResponse> generateResponse(List<Transaction> data, String timestamp) {

        RestDataResponse response = new RestDataResponse();

        if (data != null) {

            response.setVersion(new ResponseVersion(timestamp));
            response.setData(data);

            return ResponseEntity.ok().body(response);

        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public static ResponseEntity<RestDataResponse> generateResponse(RestDataResponse response, HttpStatus status) {

        if (response.getError().getCode().equals("")) {
            return ResponseEntity.ok()
                    .body(response);

        }
        return ResponseEntity.status(status)
                .body(response);
    }

    public static ResponseEntity<RestDataResponse> generateInternalServerError(String code, Exception e) {

        RestDataResponse response = new RestDataResponse();
        response.setError(new ResponseError(code, e.getMessage()));

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(response);

    }

    public static ResponseEntity<RestDataResponse> generateGenericError(String code, String description) {
        RestDataResponse response = new RestDataResponse();
        response.setError(new ResponseError(code, description));

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(response);
    }
}
