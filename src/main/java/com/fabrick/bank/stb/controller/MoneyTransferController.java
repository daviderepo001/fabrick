package com.fabrick.bank.stb.controller;

import com.fabrick.bank.stb.entity.MoneyTransfer;
import com.fabrick.bank.stb.json.MoneyTransferRequest;
import com.fabrick.bank.stb.json.MoneyTransferResponse;
import com.fabrick.bank.stb.model.RestDataResponse;
import com.fabrick.bank.stb.operations.OperationMoneyTransfers;
import com.fabrick.bank.stb.utility.CommonResponseHandler;
import com.fabrick.bank.stb.utility.Converter;
import com.fabrick.bank.stb.utility.UnirestWrapperMock;
import com.mongodb.client.MongoClient;
import lombok.extern.log4j.Log4j;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j
@RestController
public class MoneyTransferController {

    @Value("${spring.data.mongodb.database}")
    private String db;

    @Value("${spring.data.mongodb.collection.moneytransfer}")
    private String collection;

    private static String DOCUMENT_NAME = "bonifico";

    private OperationMoneyTransfers operationMoneyTransfers;

    @Autowired
    MongoClient mongoClient;

    @PostMapping("/moneyTransfer")
    public ResponseEntity<RestDataResponse> postMoneyTransfer(@RequestParam(value = "accountId") long accountId, @ModelAttribute MoneyTransferRequest body) {

        log.info("Start MoneyTransferController");
        operationMoneyTransfers = new OperationMoneyTransfers(new UnirestWrapperMock());
        ResponseEntity<RestDataResponse> responseEntity;
        MoneyTransferResponse response = null;

        try {
            response = operationMoneyTransfers.moneyTransfer(accountId, body);
            responseEntity = CommonResponseHandler.generateResponse(response, "");
        } catch (Exception e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        }

        MoneyTransfer moneyTransfer = new MoneyTransfer(accountId, body, response);

        Document bonifico = new Document();
        String jsonBonifico = Converter.fromObjectToJson(moneyTransfer);
        bonifico.put(DOCUMENT_NAME, jsonBonifico);

        mongoClient.getDatabase(db).getCollection(collection).insertOne(bonifico);

        return responseEntity;
    }
}