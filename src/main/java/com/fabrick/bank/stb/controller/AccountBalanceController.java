package com.fabrick.bank.stb.controller;

import com.fabrick.bank.stb.model.RestDataResponse;
import com.fabrick.bank.stb.operations.OperationAccountBalance;
import com.fabrick.bank.stb.utility.CommonResponseHandler;
import com.fabrick.bank.stb.utility.UnirestWrapperImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j
@RestController
public class AccountBalanceController {

    private OperationAccountBalance operationAccountBalance;

    @GetMapping("/account")
    public ResponseEntity<RestDataResponse> getAccount (@RequestParam(value = "accountId") long accountId) {

        log.info("Start AccountBalanceController");
        //TODO RETURN balance o availableBalance?
        operationAccountBalance = new OperationAccountBalance(new UnirestWrapperImpl());
        ResponseEntity<RestDataResponse> responseEntity;
        try {
            responseEntity = CommonResponseHandler.generateResponse(operationAccountBalance.account(accountId), "");
        } catch (UnirestException e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        } catch (JsonProcessingException e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        } catch (Exception e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        }
        return responseEntity;
    }
}