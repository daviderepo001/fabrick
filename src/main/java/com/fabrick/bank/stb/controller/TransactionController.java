package com.fabrick.bank.stb.controller;

import com.fabrick.bank.stb.json.Transaction;
import com.fabrick.bank.stb.model.RestDataResponse;
import com.fabrick.bank.stb.operations.OperationTransactions;
import com.fabrick.bank.stb.utility.CommonResponseHandler;
import com.fabrick.bank.stb.utility.Converter;
import com.fabrick.bank.stb.utility.UnirestWrapperImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.client.MongoClient;
import lombok.extern.log4j.Log4j;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j
@RestController
public class TransactionController {

    @Value("${spring.data.mongodb.database}")
    private String db;

    @Value("${spring.data.mongodb.collection.transaction}")
    private String collection;

    private static String DOCUMENT_NAME = "transazione";

    private OperationTransactions operationTransactions;

    @Autowired
    MongoClient mongoClient;

    @GetMapping("/transactions")
    public ResponseEntity<RestDataResponse> getTransactions (
            @RequestParam(value = "accountId") long accountId,
            @RequestParam(value = "fromAccountingDate") String fromAccountingDate,
            @RequestParam(value = "toAccountingDate") String toAccountingDate) {

        log.info("Start TransactionController");

        operationTransactions = new OperationTransactions(new UnirestWrapperImpl());

        ResponseEntity<RestDataResponse> responseEntity;
        List<com.fabrick.bank.stb.json.Transaction> transactions = null;

        try {
            transactions = operationTransactions.transactions(accountId, fromAccountingDate, toAccountingDate);

            if (transactions.size()>0) {
                responseEntity = CommonResponseHandler.generateResponse(transactions, "");
            } else {
                responseEntity = CommonResponseHandler.generateResponse("no transactions", "");
            }
            } catch (UnirestException e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        } catch (JsonProcessingException e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        } catch (Exception e) {
            responseEntity = CommonResponseHandler.generateGenericError(e.toString(), e.getMessage());
        }

        for (Transaction transaction:transactions) {
            //TODO prevedere il controllo se esiste già la transazione sul db
            Document transazione = new Document();
            String jsonTransazione = Converter.fromObjectToJson(transaction);
            transazione.put(DOCUMENT_NAME, jsonTransazione);

            mongoClient.getDatabase(db).getCollection(collection).insertOne(transazione);

        }

        return responseEntity;
    }
}