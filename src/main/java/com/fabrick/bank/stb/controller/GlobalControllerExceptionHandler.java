package com.fabrick.bank.stb.controller;
import com.fabrick.bank.stb.model.ResponseError;
import com.fabrick.bank.stb.model.RestDataResponse;
import com.fabrick.bank.stb.utility.CommonResponseHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@Log4j
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String ISE_ERR_DESC = "Internal Server Error";

//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UnirestException.class)
    ResponseEntity<RestDataResponse> handleUnirestException(UnirestException e) {

        RestDataResponse response = new RestDataResponse();
        String message = e.getMessage();
        response.setError(new ResponseError(ISE_ERR_DESC, message));
        log.error(ISE_ERR_DESC + message + e + e.getClass().getCanonicalName());

        return CommonResponseHandler.generateResponse(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(JsonProcessingException.class)
    ResponseEntity<RestDataResponse> handleJsonProcessingException(JsonProcessingException e) {

        RestDataResponse response = new RestDataResponse();
        String message = e.getMessage();
        response.setError(new ResponseError(ISE_ERR_DESC, message));
        log.error(ISE_ERR_DESC + message + e + e.getClass().getCanonicalName());

        return CommonResponseHandler.generateResponse(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IOException.class)
    ResponseEntity<RestDataResponse> handleIOException(IOException e) {

        RestDataResponse response = new RestDataResponse();
        String message = e.getMessage();
        response.setError(new ResponseError(ISE_ERR_DESC, message));
        log.error(ISE_ERR_DESC + message + e + e.getClass().getCanonicalName());

        return CommonResponseHandler.generateResponse(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
