package com.fabrick.bank.stb.repository;

import com.fabrick.bank.stb.entity.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String> {

    @Query(value = "{'transazioni.transactionId' : ?0 }")
    String findByTransactionId(String transactionId);

}