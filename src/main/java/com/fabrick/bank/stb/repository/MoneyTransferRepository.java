package com.fabrick.bank.stb.repository;

import com.fabrick.bank.stb.entity.MoneyTransfer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoneyTransferRepository extends MongoRepository<MoneyTransfer, String> {

}