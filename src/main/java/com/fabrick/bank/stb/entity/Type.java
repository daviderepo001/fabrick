package com.fabrick.bank.stb.entity;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Type {

    private String enumeration;
    private String value;
}
