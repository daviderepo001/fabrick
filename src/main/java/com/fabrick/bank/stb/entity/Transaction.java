package com.fabrick.bank.stb.entity;

import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {

    private String transactionId;
    private String operationId;
    @JsonDeserialize(using = DateHandler.class)
    private Date accountingDate;
    @JsonDeserialize(using = DateHandler.class)
    private Date valueDate;
//    private Type type;
    private float amount;
    private String currency;
    private String description;
}