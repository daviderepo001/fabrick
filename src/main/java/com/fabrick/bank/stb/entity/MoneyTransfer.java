package com.fabrick.bank.stb.entity;

import com.fabrick.bank.stb.json.MoneyTransferRequest;
import com.fabrick.bank.stb.json.MoneyTransferResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyTransfer {

    private long accountId;
    private MoneyTransferRequest request;
    private MoneyTransferResponse response;
}