package com.fabrick.bank.stb.json;

import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class MoneyTransferRequest {

    private Creditor creditor;
    @JsonDeserialize(using = DateHandler.class)
    private Date executionDate;
    private String uri;
    private String description;
    private float amount; //prevedere conversione in float
    private String currency;
    private boolean isUrgent; //prevedere conversione in boolean
    private boolean isInstant;//prevedere conversione in boolean
    private String feeType;
    private String feeAccountId;
    private TaxRelief taxRelief;

}