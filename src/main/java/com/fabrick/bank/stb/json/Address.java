package com.fabrick.bank.stb.json;

import lombok.*;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private String address;
    private String city;
    private String countryCode;
}
