package com.fabrick.bank.stb.json;

import com.fabrick.bank.stb.model.ResponseData;
import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class AccountBalance implements ResponseData {

    @JsonDeserialize(using = DateHandler.class)
    private Date date;
    private float balance;
    private float availableBalance;
    private String currency;

}
