package com.fabrick.bank.stb.json;

import com.fabrick.bank.stb.model.ResponseData;
import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MoneyTransferResponse implements ResponseData {

    private String moneyTransferId;
    private String status;
    private String direction;
    private Creditor creditor;
    private Debtor debtor;
    private String cro;
    private String uri;
    private String trn;
    private String description;
    @JsonDeserialize(using = DateHandler.class)
    private Date createdDatetime;
    @JsonDeserialize(using = DateHandler.class)
    private Date accountedDatetime;
    @JsonDeserialize(using = DateHandler.class)
    private Date debtorValueDate;
    @JsonDeserialize(using = DateHandler.class)
    private Date creditorValueDate;
    private Amount amount;
}
