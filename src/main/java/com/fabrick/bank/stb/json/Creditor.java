package com.fabrick.bank.stb.json;

import lombok.*;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Creditor {

    private String name;
    private Account account;
    private Address address;
}
