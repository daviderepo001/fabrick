package com.fabrick.bank.stb.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class LegalPersonBeneficiary {

    private String fiscalCode;
    private String legalRepresentativeFiscalCode;
}
