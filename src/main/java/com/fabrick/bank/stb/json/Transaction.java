package com.fabrick.bank.stb.json;

import com.fabrick.bank.stb.model.ResponseData;
import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements ResponseData {

    private String transactionId;
    private String operationId;
    @JsonDeserialize(using = DateHandler.class)
    private Date accountingDate;
    @JsonDeserialize(using = DateHandler.class)
    private Date valueDate;
    private Type type;
    private float amount;
    private String currency;
    private String description;
}
