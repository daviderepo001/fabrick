package com.fabrick.bank.stb.json;

import lombok.*;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private String accountCode;
    private String bicCode;

}
