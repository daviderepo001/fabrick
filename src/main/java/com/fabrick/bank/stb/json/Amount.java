package com.fabrick.bank.stb.json;

import com.fabrick.bank.stb.utility.DateHandler;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Amount {

    private float debtorAmount;
//    private String debtorAmount;
    private String debtorCurrency;
    private float creditorAmount;
//    private String creditorAmount;
    private String creditorCurrency;
    @JsonDeserialize(using = DateHandler.class)
    private Date creditorCurrencyDate;
    private float exchangeRate;
}
