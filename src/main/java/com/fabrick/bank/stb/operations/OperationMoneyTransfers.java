package com.fabrick.bank.stb.operations;

import com.fabrick.bank.stb.json.MoneyTransferRequest;
import com.fabrick.bank.stb.json.MoneyTransferResponse;
import com.fabrick.bank.stb.utility.Costants;
import com.fabrick.bank.stb.utility.UnirestWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import org.json.JSONObject;

@Log4j
public class OperationMoneyTransfers {

    private UnirestWrapper unirestWrapper;

    private static final String API_URL_ACCOUNT = Costants.API_URL + "accounts/";
    private static final String API_URL_MONEY_TRANSFERS = "/payments/money-transfers";

    public MoneyTransferResponse moneyTransfer (long accountId, MoneyTransferRequest body) throws Exception {

        String url = Costants.HOST_URL + API_URL_ACCOUNT + accountId + API_URL_MONEY_TRANSFERS;
        log.info("URL: " + url);
        log.info("Body: " + body);
        JSONObject jsonObject = unirestWrapper.post(url, body);
        log.info("Result: " + jsonObject.toString());
        MoneyTransferResponse response = new ObjectMapper().readValue(jsonObject.toString(), MoneyTransferResponse.class);
            return response;
    }

    public OperationMoneyTransfers (UnirestWrapper wrapper) {
        this.unirestWrapper = wrapper;
    }
}