package com.fabrick.bank.stb.operations;

import com.fabrick.bank.stb.json.AccountBalance;
import com.fabrick.bank.stb.utility.Converter;
import com.fabrick.bank.stb.utility.Costants;
import com.fabrick.bank.stb.utility.UnirestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.log4j.Log4j;

@Log4j
public class OperationAccountBalance {

    private UnirestWrapper unirestWrapper;

    public static final String API_URL_ACCOUNT_BALANCE_START = Costants.API_URL + "accounts/";
    public static final String API_URL_ACCOUNT_BALANCE_END = "/balance";

    public AccountBalance account(long accountId) throws Exception {

        String url = Costants.HOST_URL + API_URL_ACCOUNT_BALANCE_START + accountId + API_URL_ACCOUNT_BALANCE_END;
        log.info("URL: " + url);
        String result;
        result = unirestWrapper.get(url, 11);
        log.info("Result: " + result);
        try {
            AccountBalance accountBalance = new ObjectMapper().readValue(result, AccountBalance.class);
            return accountBalance;
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public OperationAccountBalance (UnirestWrapper wrapper) {
        this.unirestWrapper = wrapper;
    }
}