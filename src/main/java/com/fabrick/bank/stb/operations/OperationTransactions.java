package com.fabrick.bank.stb.operations;

import com.fabrick.bank.stb.json.Transaction;
import com.fabrick.bank.stb.utility.Costants;
import com.fabrick.bank.stb.utility.UnirestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.log4j.Log4j;

import java.util.LinkedList;
import java.util.List;

@Log4j
public class OperationTransactions {

    private UnirestWrapper unirestWrapper;

    public static final String API_URL_ACCOUNT = Costants.API_URL + "accounts/";
    public static final String API_URL_TRANSACTIONS = "/transactions";

    public List<Transaction> transactions(long accountId, String fromAccountingDate, String toAccountingDate) throws Exception {

        String url = Costants.HOST_URL + API_URL_ACCOUNT + accountId + API_URL_TRANSACTIONS + "?" + "fromAccountingDate=" + fromAccountingDate + "&" + "toAccountingDate=" + toAccountingDate;
        log.info("URL: " + url);
        String result = unirestWrapper.get(url, 19);
        log.info("Result: " + result);
        List<Transaction> transactionList = new LinkedList<>();
        transactionList = new ObjectMapper().readValue(result, new ObjectMapper().getTypeFactory().constructCollectionType(List.class, Transaction.class));
        return transactionList;
    }

    public OperationTransactions(UnirestWrapper wrapper) {
        this.unirestWrapper = wrapper;
    }
}