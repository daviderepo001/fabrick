/**
 *
 */

package com.fabrick.bank.stb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseError {

    private String code;
    private String description;

}
