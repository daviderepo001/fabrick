/**
 *
 */

package com.fabrick.bank.stb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class ResponseVersion {

    private String timestamp;
}
