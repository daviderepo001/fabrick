/**
 *
 */

package com.fabrick.bank.stb.model;

import lombok.Data;

/**
 *
 */
@Data
public class RestDataResponse<T> {

    private ResponseError error;
    private ResponseVersion version;
    private T data;

    public RestDataResponse() {
        this.error = null;
        this.version = null;
    }

}
