package com.fabrick.bank.stb.operations;

import com.fabrick.bank.stb.json.Transaction;
import com.fabrick.bank.stb.operations.OperationTransactions;
import com.fabrick.bank.stb.utility.UnirestWrapperImpl;
import com.fabrick.bank.stb.utility.UnirestWrapperMock;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class OperationTransactionsIntegrationTest {

    OperationTransactions operationTransactions;

        @Before
        public void setUp () {
            operationTransactions = new OperationTransactions(new UnirestWrapperImpl());
        }

        @SneakyThrows
        @Test
        public void transactionTestWithAccountId () {

            long accountId = 14537780;
            String fromAccountingDate = "2019-01-01";
            String toAccountingDate = "2019-12-31";
            String expectedTransactionId = "519157";
            List<Transaction> result = operationTransactions.transactions(accountId, fromAccountingDate, toAccountingDate);
            Assert.assertEquals(result.get(0).getTransactionId(), expectedTransactionId);
        }

        @SneakyThrows
        @Test(expected = Exception.class)
        public void transactionTestWithoutAccountId () {

            long accountId = 0;
            String fromAccountingDate = "2019-01-01";
            String toAccountingDate = "2019-12-31";
            operationTransactions.transactions(accountId, fromAccountingDate, toAccountingDate);
        }
}